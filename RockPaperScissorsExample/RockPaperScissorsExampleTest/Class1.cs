﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using RockPaperScissorsExample;
using RockPaperScissorsExample.Enums;
using RockPaperScissorsExample.Implementations;
using RockPaperScissorsExample.interfaces;



namespace RockPaperScissorsExampleTest
{

    public class Class1
    {
        [TestFixture]
        public class RockPaperScissorsExample
        {
           

            [TestCase(Choice.Scissors, Result.Win)]
            [TestCase(Choice.Rock, Result.Loss)]
            [TestCase(Choice.Paper, Result.Tie)]

            public void AlwaysPaper(Choice a,  Result b)
            {
               TestGame target = new TestGame();

                Result actual = target.PlayRound(a);

                Assert.AreEqual(b, actual);

            }


        }

    }
}
